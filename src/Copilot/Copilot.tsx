import React from "react"
import Header from "../Components/Header/Header"
import Dashboard from "./Dashboard/Dashboard"

const Copilot: React.FC = () => {
  return (
    <div>
      <Header title="Copilot" type="" />
      <div className="copilot-db-header">
        <div className="copilot-db-title">
          Copilot Changes
        </div>
      </div>
      <div className="copilot-dashboard">
        <Dashboard />
      </div>
      <div/>
    </div>
  )
}

export default Copilot
