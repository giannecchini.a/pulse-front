import React, { Fragment } from "react"
import Row from '../../Components/InfoGrid/Row/Row'
import Title from '../../Components/InfoGrid/Title/Title'

export interface IObjectFlight {
  label: string,
}

export interface IObjectInfo {
  advertiser: string,
  agency: string,
  adType: string
  campaign: string
  flight: IObjectFlight
  objective: string
}

const InfoCampaign = (props: IObjectInfo) => {
  return (
    <div className="info-data">
      {props && (
        <Fragment>
          <Title title={props.campaign} />
          <Row title="Agency" detail={props.agency} />
          <Row title="Advertiser" detail={props.advertiser} />
          <Row title="Ad type" detail={props.adType} />
          <Row title="Flight" detail={props.flight.label} />
          <Row title="Objective" detail={props.objective} />
        </Fragment>
      )}
    </div>
  )
}

export default InfoCampaign
