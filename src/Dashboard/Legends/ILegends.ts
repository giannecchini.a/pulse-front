import * as enums from '../../utils/enums'

export interface ILegendsProps {
  callback?: any
}

export const LegendsConst  = {
  aiOptimization : {
    color: enums.AIOptimization.class,
    id: 5,
    legendDesc: 'Tenemos un mix de algoritmos y modelos de aprendizaje preparados.',
    title : enums.AIOptimization.title,
  },
  all: {
    color: 'blue',
    id: 0,
    title : 'All',
  },
  brandSafety : {
    color: enums.BrandSafety.class,
    id: 4,
    legendDesc: 'Seleccionamos los mejores medios y espacios para servir la pauta de acuerdo al objetivo y el target.',
    title : enums.BrandSafety.title,
  },
  deliveryPacing : {
    color: enums.DeliveryPacing.class,
    id: 2,
    legendDesc: 'Las optimizaciones de delivery buscan llegar al punto deseado de escala sobre la estrategia.',
    title : enums.DeliveryPacing.title,
  },
  initialRequirements : {
    color: enums.InitialRequirements.class,
    id: 1,
    legendDesc: 'Estas son los requerimientos básicos de la campaña, incluyen fechas, presupuesto y formatos.',
    title : enums.InitialRequirements.title,
  },
  segmentation : {
    color: enums.Segmentation.class,
    id: 3,
    legendDesc: 'Los cambios en la segmentación ayudan a buscar y aprender sobre los consumidores que conoces, y los que hay por descubrir.',
    title : enums.Segmentation.title,
  },
}

export type Legends = keyof typeof LegendsConst