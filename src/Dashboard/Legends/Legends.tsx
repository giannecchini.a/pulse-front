import React, { Fragment } from "react"
import Legend from '../../Components/Legend/Legend'
import { ILegendsProps, LegendsConst } from './ILegends'
import * as enums from '../../utils/enums'

const Legends = (props: ILegendsProps) => {
  const { callback  } = props
  return (
    <div className="pulse-content-legend">
        <Fragment>
          <Legend
            id={LegendsConst.initialRequirements.id}
            title={LegendsConst.initialRequirements.title}
            color={LegendsConst.initialRequirements.color}
            tooltip={{ text: LegendsConst.initialRequirements.legendDesc, color: '' }}
          />
          <Legend
            id={LegendsConst.deliveryPacing.id}
            title={LegendsConst.deliveryPacing.title}
            color={LegendsConst.deliveryPacing.color}
            tooltip={{ text: LegendsConst.deliveryPacing.legendDesc, color: '' }}
          />
          <Legend
            id={LegendsConst.segmentation.id}
            title={LegendsConst.segmentation.title}
            color={LegendsConst.segmentation.color}
            tooltip={{ text: LegendsConst.segmentation.legendDesc, color: '' }}
          />
          <Legend
            id={LegendsConst.brandSafety.id}
            title={LegendsConst.brandSafety.title}
            color={LegendsConst.brandSafety.color}
            tooltip={{ text: LegendsConst.brandSafety.legendDesc, color: '' }}
          />
          <Legend
            id={LegendsConst.aiOptimization.id}
            title={LegendsConst.aiOptimization.title}
            color={LegendsConst.aiOptimization.color}
            tooltip={{ text: LegendsConst.aiOptimization.legendDesc, color: '' }}
          />
        </Fragment>
    </div>
  )
}

export default Legends
