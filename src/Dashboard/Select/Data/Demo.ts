
const optionsExport = [
  { value: 'CSV', label: 'CSV' },
  { value: 'PPT', label: 'PPT' },
  { value: 'PDF', label: 'PDF' },
]
const optionsMarket = [
	{value : "CL" , label:"Chile"},
	{value : "AR" , label:"Argentina"},
	{value : "MIA" , label:"Miami"},
	{value : "MX" , label:"Mexico"},
	{value : "CO" , label:"Colombia"},
	{value : "PE" , label:"Peru"},
	{value : "UY" , label:"Uruguay"},
	{value : "PR" , label:"Puerto Rico"	},
]

const optionsAdvertiser =[
	{value:"SO",label:"Sodimac"}
]

export {
  optionsExport,
  optionsMarket,
  optionsAdvertiser
}