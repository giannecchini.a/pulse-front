
export interface ISelectProps {
  lineItems: any[],
  flights: any[]
  callbackCampaign?: any
}

export interface ISelect {
  id: number
  value: string
  label: string
  flight: any
  shortName: string
  objective: string
  adType: string
  agency: string
}

export interface ISelectState {
  market?: ISelect
  advertiser?: ISelect
  campaign?: ISelect
  toExport?: ISelect
  flight?: ISelect
  callback?: boolean
}

export enum TypeSelects {
  market= 'market',
  campaign= 'campaign',
  export= 'export',
  advertiser= 'advertiser',
  flight= 'flight'
}