import React from "react"
import Select from 'react-select'
import {ISelectProps, ISelect, ISelectState, TypeSelects } from './ISelect'
import { optionsExport, optionsMarket,optionsAdvertiser} from './Data/Demo'

export default class SelectCampaign extends React.Component<ISelectProps,ISelectState> {

  constructor(props: ISelectProps) {
    super(props)
    const { lineItems } = props
    const firstOption = lineItems.length > 0 && lineItems[0]
    const flightSelect = firstOption && firstOption.flight
    this.state = {
      advertiser: {value: "SO", label: "Sodimac", flight: {}, shortName: '', objective: '', agency: '', adType: '', id: 0},
      campaign: firstOption,
      flight: flightSelect,
      market: {value: "CL", label: "Chile", flight: {}, shortName: '', objective: '', agency: '', adType: '', id: 0},
      toExport: {value: "", label: "", flight: {}, shortName: '', objective: '', agency: '', adType: '', id: 0},
    }
  }

  /**
   * change state and react-select
   */
  onChangeSelect = (type: TypeSelects, value: ISelect) => {
    const { callbackCampaign } = this.props
    const { advertiser,campaign } = this.state
    const advertiserSelect = advertiser && advertiser.label
    switch (type) {
      case TypeSelects.market:
        this.setState({market: value})
      break
      case TypeSelects.campaign:
        const flightSelect = value && value.flight
        this.setState({campaign: value, flight: flightSelect })
        callbackCampaign(value, flightSelect, advertiserSelect)
      break
      case TypeSelects.flight:
        this.setState({flight: value})
        callbackCampaign(campaign, value, advertiserSelect)
      break
      case TypeSelects.export:
      this.setState({toExport: value})
      break
    }
  }
  /**
   * validation according to type select
   */
  validaionValue = (type: TypeSelects) => {
    const { market, advertiser, campaign, toExport, flight } = this.state
    let value = null
    switch (type) {
      case TypeSelects.market:
        value = market && market.value !== "" ? {value: market && market.value, label: market && market.label} : null
      break
      case TypeSelects.advertiser:
        value = advertiser && advertiser.value !== "" ? {value: advertiser && advertiser.value, label: advertiser && advertiser.label} : null
      break
      case TypeSelects.campaign:
      value = campaign && campaign.value !== "" ? {value: campaign && campaign.value, label: campaign && campaign.label} : null
      break
      case TypeSelects.export:
      value = toExport && toExport.value !== "" ? {value: toExport && toExport.value, label: toExport && toExport.label} : null
      break
      case TypeSelects.flight:
      value = flight && flight.value !== "" ? {value: flight && flight.value, label: flight && flight.label} : null
      break
    }
    return value
  }

  render() {
    const { lineItems, flights } = this.props
    const optionsCampaign = lineItems.length > 0 ? lineItems : []
    const flightsCampaign = flights.length > 0 ? flights : []
    return (
      <div className="info-select">
        <div className="info-width">
          <div className="info-select-button">
            <Select
              className="select-info export"
              placeholder="Market"
              value={this.validaionValue(TypeSelects.market)}
              options={optionsMarket}
              onChange={(data: any) => {this.onChangeSelect(TypeSelects.market, data)}}
            />
            <Select
              className="select-info export"
              placeholder="Advertiser"
              value={this.validaionValue(TypeSelects.advertiser)}
              options={optionsAdvertiser}
            />
          </div>
        </div>
        <div className="info-width">
          <div className="info-select-button">
            <Select
              className="select-info export"
              value={this.validaionValue(TypeSelects.campaign)}
              options={optionsCampaign}
              placeholder="Select your campaign"
              onChange={(data: any) => {this.onChangeSelect(TypeSelects.campaign, data)}}
            />
            <Select
              className="select-info export"
              placeholder="Flight"
              value={this.validaionValue(TypeSelects.flight)}
              options={flightsCampaign}
              onChange={(data: any) => {this.onChangeSelect(TypeSelects.flight, data)}}
            />
          </div>
        </div>
        <div className="info-width">
          <div className="info-select-button">
            <Select
              className="select-info export"
              placeholder="Export to"
              value={this.validaionValue(TypeSelects.export)}
              options={optionsExport}
              onChange={(data: any) => { this.onChangeSelect(TypeSelects.export, data)  }}
            />
          </div>
        </div>
      </div>
    )
  }
}
