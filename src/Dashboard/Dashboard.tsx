import React from "react"
import Header from "../Components/Header/Header"
import SelectCampaign from './Select/Select'
import InfoCampaign from './Campaign/InfoCampaign'
import PieChart from './Pulse/PieChart'
import LineChart from './Pulse/LineChart'
import Copilot from './Copilot/Index'
import Tips from './Tips/Tips'
import { IDashboardState } from './IDashboard'
import { ISelect } from './Select/ISelect'
import ChangeLog from './ChangeLog/ChangeLog'
import Loader from '../Components/Loader/Loader'
import InitialLoader from '../Components/Loader/InitialLoader'
import Menu from "../Components/Menu/Menu"

const URL = 'https://api.xaxispulse.com/api/'

export default class Dashboard extends React.Component<any,IDashboardState> {

  constructor(props: any) {
    super(props)
    this.state = {
      changeLogPulse: [],
      changeLogShow: {},
      dataPulse: [],
      flights: [],
      flightsOptions: [],
      labels: [],
      lineItems: [],
      loader: false ,
      objectSelect: {
        adType: '',
        advertiser: '',
        agency : '',
        campaign: '',
        flight: {
          label: '',
        },
        objective: ''
      },
    }
  }

  componentDidMount = () => {
    this.getinitialData()
  }

  /**
   * initial data of campaign
   */
  getinitialData = async () => {
    try {
      const response = await fetch(`${URL}Dashboard/LoadDataDefault`)
      if (response.ok) {
        const json = await response.json()
        const lineItem = json.lineItems[0]
        const objectSelect = {
          adType: lineItem ? lineItem.adType : '',
          advertiser: 'Sodimac',
          agency : lineItem ? lineItem.agency : '',
          campaign: lineItem.shortName,
          flight: {
            label:  lineItem.flight.label,
          },
          objective: lineItem.objective
        }
        const stateChange = {
          flights: json.flights,
          lineItems: json.lineItems,
          objectSelect,
        }
        this.getPulseData(lineItem.id,lineItem.flight.id, stateChange)
      }
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error)
    }
  }

  /**
   * Select data Pulse
   */
  getPulseData = async (lineItemId: number, flightId: number, stateChange: object = {}) => {
    try {
      const response = await fetch(`${URL}Dashboard/GetDataForGraphingByLineItemAndFlight?LineItemId=${lineItemId}&Flight=${flightId}`)
      if (response.ok) {
        const json = await response.json()
        const countChange = json.changeLogCharts.length
        let arrayLogShow: any[] = []
        if (countChange > 0) {
          json.changeLogCharts.map((change: any) => {
            arrayLogShow = change.changeTraduction.length > 0 ? change : arrayLogShow
          })
        }
        this.setState({
          changeLogPulse: json.changeLogCharts,
          changeLogShow: arrayLogShow,
          dataPulse: json.typeOfOptimizationChartDTO,
          labels: json.labels,
          ...stateChange
         })
      }
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error)
    }
  }

  /**
   * Flight Data
   */
  getFlightData = async (select: ISelect ) => {
    try {
      const response = await fetch(`${URL}Dashboard/GetFlightByLineItemId/${select.id}`)
      if (response.ok) {
        const json = await response.json()
        this.setState({
          flights: json,
         })
      }
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error)
    }
  }

  /**
   * update data select - chart
   */
  updateData = async (select: ISelect, flight: ISelect, objSelect: any ) => {
    try {
      await this.setState({objectSelect: objSelect})
      await this.getFlightData(select)
      await this.getPulseData(select.id,flight.id,{ loader: false }  )
    } catch (error) {
      return false
    }
  }

  /**
   * callback select campaign
   */
  callbackCampaign = async (select: ISelect, flight: ISelect, advertiser: string ) => {
    try {
      this.setState({loader: true })
      const objSelect = {
        adType: select ? select.adType : '',
        advertiser,
        agency : select ? select.agency : '',
        campaign: select.shortName,
        flight: {
          label:  flight.label,
        },
        objective: select.objective
      }
      await this.updateData(select, flight, objSelect)
    } catch (error) {
        // tslint:disable-next-line:no-console
        console.error(error)
    }
  }

  callbackPulseClick = (indexArray: number) => {
    const { changeLogPulse, labels } = this.state
    this.setState({
      changeLogShow:changeLogPulse[indexArray],
    })
  }

  render() {
    const {
      dataPulse,
      labels,
      lineItems,
      objectSelect,
      flights,
      loader,
      changeLogShow,
    } = this.state

    if (dataPulse.length === 0) {return <InitialLoader /> }
    return (
      <div className="content" >
        <Header title="Dashboard" type="" />
        <Menu />
        <Loader modalIsOpen={loader} />
        <div className="panel-content" >
          <div className="dash-container">
            <div>
              <SelectCampaign
                lineItems={lineItems}
                flights={flights}
                callbackCampaign={(select: ISelect, flight: ISelect, advertiser: string) => this.callbackCampaign(select, flight, advertiser)}
              />
              <InfoCampaign {...objectSelect} />
              <PieChart dataPulse={dataPulse} />
            </div>
            <div className="pulse-container chart">
              <LineChart
                dataPulse={dataPulse}
                labels={labels}
                callback={(index: number) => {this.callbackPulseClick(index)}}
              />
              <div className="space" />
              <div className="space" />
              <ChangeLog changeLog={changeLogShow}/>
            </div>
          </div>
          <Copilot />
        </div>
        <Tips />
      </div>
    )
  }
}
