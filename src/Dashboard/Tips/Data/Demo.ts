
const demoTips = [
  'Se recomienda cambiar el presupuesto de las estrategias de bajo rendimiento a otras que presenten mejor actividad y tengan potencial para llegar al objetivo.',
  'Probar hipotesis de audiencias en las campañas para descubrir usuarios potenciales.',
  'Permitir a la plataforma ser flexible entre diferentes clusters de audiencias, para así optimizar hacia aquellos que tengan mejor desempeño.',
  'Aprovechar los desarrollos de las plataformas de compra para acceder a tecnicas actualizadas de targeting.',
  'Diversificar fuentes de inventario fuera de RTB / Open exchange con el fin de aprovechar otro tipo de fuentes tales como los private market places.',
  'Introducir creativos variados para un mayor alcance y posibilidades de optimización.',
  'Para comparar el efecto que diferentes estrategias tienen sobre el objetivo, se recomienda llevar a cabo un A / B Testing.',
  'Se aconseja identificar lapsos de tiempo en los cuales no se está presentando un buen desempeño y enfocar la optimizacion a diferentes horarios.',
  'Se cuenta con herramientas de verificación de anuncios (AVT) aplicadas a las campañas para un monitoreo constante que permita  alzar cualquier alarma de sitios no seguros o fraudulentos.',
  'Para que un anuncio genere valor primero debe ser visto por el consumidor. Si bien no necesariamente es el KPI principal, el viewability ha sido reconocido como un factor cada vez más importante a tener en cuenta.',
  'Todas las campañas cuentan con blacklists y filtros pre-bid de Brand Safety para evitar el riesgo de salir en contenido no apropiado.',
  'Usar diferentes fuentes de data puede ayudar a potencializar la estrategia, sacando provecho de las capacidades de nuestro DMP en las campañas.',
  'Para campañas que buscan conversiones, los formatos nativos son altamente recomendados. Por su naturaleza pueden impactar usuarios que ya han mostrado un interés en la marca o producto.',
  '¿Algo no está funcionando como debería? Intenta mover tu inversión a estrategias que puedan sacarle mejor provecho.',
  'La compra programática con un uso eficiente de data puede llevarte a consumidores que no conocias.',
  '“Si doblas el numero de experimentos que haces al año, vas a doblar tu capacidad de inventar cosas nuevas” Jeff Bezos, CEO y fundador de Amazon.',
  '“Lo que es peligroso es no evolucionar” Jeff Bezos, CEO y fundador de Amazon.',
  'Utiliza herramientas de analisis de audiencias para descubrir más sobre tus consumidores.',
  'La data y la tecnología van a un ritmo acelerado, evolucionar en la misma medida nos permite una ventaja competitiva.',
]

export default demoTips
