import React from "react"
import demoTips from './Data/Demo'

interface ITipsState {
  message : string
  fade: boolean
}

export default class LoadingTips extends React.Component<any,ITipsState> {

  constructor(props: any) {
    super(props)
    this.state = {
      fade: false,
      message: demoTips[Math.floor(Math.random() * demoTips.length)],
    }
  }

  componentDidMount = () => {
    setInterval(() => {
     this.changeMessage()
    },10000)
  }

  changeMessage = () => {
    const randomText = demoTips[Math.floor(Math.random() * demoTips.length)]
    this.setState({ message: randomText, fade: !this.state.fade  })
  }

  render() {
    const { message } = this.state
    const className = 'fade' + (this.state.fade ? ' fade-enter' : '')
    return (
      <div className="random-text" >
        <div className={className}>
          {message}
        </div>
      </div>
    )
  }

}
