import React from "react"
import ReactCSSTransition from 'react-addons-css-transition-group'
import demoTips from './Data/Demo'

interface ITipsState {
  message : string
}

export default class Tips extends React.Component<any,ITipsState> {

  constructor(props: any) {
    super(props)
    this.state = {
      message: demoTips[Math.floor(Math.random() * demoTips.length)]
    }
  }

  componentDidMount = () => {
    setInterval(() => {
     this.changeMessage()
    },6000)
  }

  changeMessage = () => {
    const randomText = demoTips[Math.floor(Math.random() * demoTips.length)]
    this.setState({ message: randomText })
  }

  componentOne = (message: string) => (
    <div className="pulse-help">
      {message}
    </div>
  )

  render() {
    const { message } = this.state
    const currentComponent = this.componentOne(message)

    return (
      <ReactCSSTransition
        transitionName="example"
        transitionEnterTimeout={800}
        transitionLeaveTimeout={800}
      >
        <div className="pulse-help-content" >
            {currentComponent}
        </div>
      </ReactCSSTransition>
    )
  }

}
