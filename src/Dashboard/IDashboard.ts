import { IObjectInfo } from './Campaign/InfoCampaign'

export interface IDataDash {
  data: number[]
  code: string
  legend: string
  percent: string
}

export interface IDashboardState {
  changeLogPulse: any[]
  changeLogShow: any
  dataPulse: any[]
  flights: any[]
  flightsOptions: any[]
  labels: any
  lineItems: any[]
  loader: boolean
  objectSelect: IObjectInfo
}
