import * as React from "react"
import { Line } from "react-chartjs-2"
import { IDashProps, IDashState } from './ICharts'
import * as enums from '../../utils/enums'
import Legends from '../Legends/Legends'

export default class LineChart extends React.Component<IDashProps,IDashState> {

  static getDerivedStateFromProps(props: IDashProps, state: IDashState) {
    const returnStates = {
      dataPulse: props.dataPulse,
      labels: props.labels
    }
    return returnStates
  }

  constructor(props: IDashProps) {
    super(props)
    this.state = {
      dataPulse: props.dataPulse,
      labels: props.labels,
      showLegend: false ,
    }
  }

  getOptions = (scale: number = 10) => {
    const options = {
      events: ['mousemove'],
      hover: {
        mode: "point",
      },
      legend: {
        display: false ,
        onHover: (e: any) => {
          e.target.style.cursor = 'pointer'
       },
      },
      maintainAspectRatio: false ,
      onHover: (event: any, chartElement: any) => {
        event.target.style.cursor = 'pointer'
      },
      responsive: true,
      scales: {
        xAxes: [
          {
            display: true,
            scaleLabel: {
              labelString: "Month",
              show: true,
            },
            ticks: {
              stepSize: 2,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              stepSize: 1,
              suggestedMax: 10,
              suggestedMin: 0,
            },
          },
        ],
      },
      tooltips: {
        mode: "label",
        position: 'nearest',
      },
    }
    return options
  }

  reloadChart = () => {
    const { labels, dataPulse } = this.state
    const dataset: object[]  = []
    if (dataPulse.length > 0) {
      let color = ''
      let name = ''
      dataPulse.map((data: any) => {
        switch (data.code) {
          case enums.InitialRequirements.code:
            color = enums.InitialRequirements.color
            name = enums.InitialRequirements.title
          break
          case enums.AIOptimization.code:
            color = enums.AIOptimization.color
            name = enums.AIOptimization.title
          break
          case enums.BrandSafety.code:
            color = enums.BrandSafety.color
            name = enums.BrandSafety.title
          break
          case enums.DeliveryPacing.code:
            color = enums.DeliveryPacing.color
            name = enums.DeliveryPacing.title
          break
          case enums.Segmentation.code:
            color = enums.Segmentation.color
            name = enums.Segmentation.title
          break
        }
        const item = {
          backgroundColor: color,
          borderCapStyle: "round",
          borderColor: color,
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          borderWidth: 4,
          data: data.data,
          fill: false ,
          label: name,
          lineTension: 0.1,
          pointBackgroundColor: color,
          pointBorderColor: color,
          pointBorderWidth: 1,
          pointHitRadius: 10,
          pointHoverBackgroundColor: color,
          pointHoverBorderColor: color,
          pointHoverBorderWidth: 2,
          pointHoverRadius: 5,
          pointRadius: 3,
        }
        return dataset.push(item)
      })
    }

    return {
      datasets: dataset,
      labels,
    }
  }

  render() {
    const { showLegend } = this.state
    const { callback } = this.props
    return(
      <React.Fragment>
       <Legends />
        <div className="pulse-dash">
        <div className="pulse-dash-container" >
          <Line
            data={this.reloadChart()}
            options={this.getOptions(5)}
            onElementsClick={(elems) => {
              const indexArray = elems[0] && elems[0]._index
              if (indexArray >= 0) {
                callback(indexArray)
              }
            }}
          />
        </div>
        </div>
        {showLegend && (
          <div className={`pulse-select orange`}>
            {'jajajajakakak'}
          </div>
        )}
      </React.Fragment>
    )
  }
}