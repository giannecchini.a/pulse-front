import React from "react"
import { Doughnut } from "react-chartjs-2"
import Title from '../../Components/InfoGrid/Title/Title'
import { IPieProps, IPieState } from './ICharts'
import * as enums from '../../utils/enums'


export default class PieChart extends React.Component<IPieProps,IPieState> {

  static getDerivedStateFromProps(props: IPieProps, state: IPieState) {
    if (props.dataPulse !== state.dataPulse) {
      return {
        dataPulse: props.dataPulse,
      }
    }
    return null
  }

  constructor(props: IPieProps) {
    super(props)
    this.state = {
      dataPieChart: [],
      dataPulse: [],
    }
  }

  getOptions = () => {
    const { dataPulse } = this.state
    const dataPieTest = {
      AIOptimization: 0,
      BrandSafety: 0,
      DeliveryPacing: 0,
      InitialRequirements: 0,
      Segmentation: 0,
    }

    if (dataPulse.length > 0) {
      dataPulse.map((data: any) => {
        switch (data.code) {
          case enums.InitialRequirements.code:
          dataPieTest.InitialRequirements = data.percent
          break
          case enums.AIOptimization.code:
          dataPieTest.AIOptimization = data.percent
          break
          case enums.BrandSafety.code:
          dataPieTest.BrandSafety = data.percent
          break
          case enums.DeliveryPacing.code:
          dataPieTest.DeliveryPacing = data.percent
          break
          case enums.Segmentation.code:
          dataPieTest.Segmentation = data.percent
          break
        }
      })
    }
    const dataPie = {
      datasets: [{
        backgroundColor: [
          enums.InitialRequirements.color,
          enums.DeliveryPacing.color,
          enums.Segmentation.color,
          enums.BrandSafety.color,
          enums.AIOptimization.color,
          ],
        data: [
          dataPieTest.InitialRequirements,
          dataPieTest.DeliveryPacing,
          dataPieTest.Segmentation,
          dataPieTest.BrandSafety,
          dataPieTest.AIOptimization,
        ],
        hoverBackgroundColor: [
          enums.InitialRequirements.color,
          enums.DeliveryPacing.color,
          enums.Segmentation.color,
          enums.BrandSafety.color,
          enums.AIOptimization.color,
        ]
      }],
      labels: [
        enums.InitialRequirements.title,
        enums.DeliveryPacing.title,
        enums.Segmentation.title,
        enums.BrandSafety.title,
        enums.AIOptimization.title,
      ],
    }
    const options = {
      hover: {
        mode: "dataset",
      },
      legend: false ,
      maintainAspectRatio: false ,
      position: 'left',
      responsive: true,
      tooltips: {
        mode: "label",
      },
    }

    return {
      dataPie,
      dataPieTest,
      options,
    }
  }

  render() {
    const pieData = this.getOptions()
    return (
      <div className="info-dash">
        <Title title="Category Changes" />
        <div className="space" />
        <div className="info-chart">
          <div className="chart-pie info">
            <div className="legend-color" >
              <div className={`color ${enums.InitialRequirements.class} large`}/>
              <div className="space" />
              <div>{`${pieData.dataPieTest.InitialRequirements || '0'}%`}</div>
            </div>
            <div className="legend-color" >
              <div className={`color ${enums.DeliveryPacing.class} large`}/>
              <div className="space" />
              <div>{`${pieData.dataPieTest.DeliveryPacing || '0'}%`}</div>
            </div>
            <div className="legend-color" >
              <div className={`color ${enums.Segmentation.class} large`}/>
              <div className="space" />
              <div>{`${pieData.dataPieTest.Segmentation || '0'}%`}</div>
            </div>
            <div className="legend-color" >
              <div className={`color ${enums.BrandSafety.class} large`}/>
              <div className="space" />
              <div>{`${pieData.dataPieTest.BrandSafety || '0'}%`}</div>
            </div>
            <div className="legend-color" >
              <div className={`color ${enums.AIOptimization.class} large`}/>
              <div className="space" />
              <div>{`${pieData.dataPieTest.AIOptimization || '0'}%`}</div>
            </div>
          </div>
          <div className="chart-pie info">
            <div>{enums.InitialRequirements.title}</div>
            <div>{enums.DeliveryPacing.title}</div>
            <div>{enums.Segmentation.title}</div>
            <div>{enums.BrandSafety.title}</div>
            <div>{enums.AIOptimization.title}</div>
          </div>
          <div className="chart-pie">
            <Doughnut data={pieData.dataPie} options={pieData.options} />
          </div>
        </div>
      </div>
    )
  }
}

