
export interface IDashProps {
  dataPulse: any,
  labels: any
  callback: any
}

export interface IDashState {
  dataPulse: any,
  labels: any
  showLegend: boolean,
}

export interface IPieProps {
  dataPulse: any,
}

export interface IPieState {
  dataPulse: any,
  dataPieChart: any
}