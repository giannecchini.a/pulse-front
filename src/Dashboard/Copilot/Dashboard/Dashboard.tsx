import * as React from "react"
import { Line } from "react-chartjs-2"
import { arrayDays } from "../../../utils/utils"
import { strategies } from "../Data/Demo"

export default class DashboardCopilot extends React.Component<{},{}> {

  getOptions = () => {
    const options = {
      hover: {
        mode: "dataset",
      },
      maintainAspectRatio: false ,
      responsive: true,
      scales: {
        xAxes: [
          {
            display: true,
            ticks: {
              stepSize: 2,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              stepSize: 1,
              suggestedMax: 3,
              suggestedMin: 0,
            },
          },
        ],
      },
      tooltips: {
        mode: "label",
      },
    }
    return options
  }

  render() {
    // tslint:disable-next-line:array-type
    const dataset: Array<object> = []
    // tslint:disable-next-line:no-shadowed-variable
    strategies.map((data) => {
      const item = {
        backgroundColor: data.color,
        borderCapStyle: "round",
        borderColor: data.color,
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        borderWidth: 4,
        data: data.data,
        fill: false ,
        label: data.name,
        lineTension: 0.1,
        pointBackgroundColor: data.color,
        pointBorderColor: data.color,
        pointBorderWidth: 1,
        pointHitRadius: 10,
        pointHoverBackgroundColor: data.color,
        pointHoverBorderColor: data.color,
        pointHoverBorderWidth: 2,
        pointHoverRadius: 5,
        pointRadius: 3,
      }
      return dataset.push(item)
    })

    const data = {
      datasets: dataset,
      labels: arrayDays(),
    }

    return(
      <Line data={data} options={this.getOptions()} />
    )
  }
}
