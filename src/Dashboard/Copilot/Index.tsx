import React from "react"
import Title from '../../Components/InfoGrid/Title/Title'
import CopilotChart from './Dashboard/Dashboard'

const Index: React.FC = () => {
  return (
  <div className="dash-container copilot">
    <div className="pulse-container info">
    <div className="space" />
      <div className="info-copilot">
        <div className="info-width">
            <Title title="Copilot Strategies" />
            <div className="copilot-strategies">
              <div className="copilot-image" />
            </div>
        </div>
      </div>
    </div>
    <div className="pulse-container chart">
      <div className="pulse-dash">
        <div className="pulse-dash-container" >
          <Title title="Copilot Changes" />
          <div className="space" />
          <div className="space" />
            <CopilotChart />
        </div>
      </div>
    </div>
  </div>
  )
}

export default Index
