import React from "react"
import * as enums from '../../utils/enums'

interface ILogProps {
  changeLog : any
}

export default class ChangeLog extends React.Component<ILogProps,{}> {

  getColor = (codeColor: string  ) => {
    switch (codeColor) {
      case enums.InitialRequirements.code:
        return enums.InitialRequirements.class
      break
      case enums.AIOptimization.code:
        return enums.AIOptimization.class
      break
      case enums.BrandSafety.code:
        return enums.BrandSafety.class
      break
      case enums.DeliveryPacing.code:
        return enums.DeliveryPacing.class
      break
      case enums.Segmentation.code:
        return enums.Segmentation.class
      break
    }
  }

  generateRows = ( dataRows: object[], date: string) => {
    let grayRow = false
    return(
      <div>
        <div className={`pulse-change-row date`}>
            <div>Change Log</div>
            <div>{date}</div>
          </div>
        <div className="pulse-log">
          <div>
            {dataRows.map((data: any,index: number) => {
              grayRow = !grayRow
              return (
                <div key={index} className={`pulse-change-row ${grayRow && 'gray'}`}>
                  {data.code && (
                    <React.Fragment>
                      <div className={`color ${this.getColor(data.code)} small`}/>
                      <div className="space" />
                    </React.Fragment>
                  )}
                  <div>{data.description} </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }

  blankRows = () => {
    const arrayDemo = []
    arrayDemo.push({ description: '' })
    for (let index = 1; index < 11; index++) {
      arrayDemo.push({ description: '' })
    }
    return this.generateRows(arrayDemo, '')
  }

  render() {
    const { changeLog } = this.props
    const dataChangeLog = changeLog && changeLog.changeTraduction
    const dateChangeLog = changeLog && changeLog.numberDate
    if (!dataChangeLog) { return (this.blankRows()) }
    if (dataChangeLog.length === 0) { return this.blankRows() }
    return this.generateRows(dataChangeLog, dateChangeLog)
  }
}
