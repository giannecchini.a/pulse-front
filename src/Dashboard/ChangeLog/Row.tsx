import React from "react"

const Row = (rowLog: string) => (
  <div className="pulse-log">
    {rowLog}
  </div>
)

export default Row