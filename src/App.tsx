import React from "react"
import { Router } from "react-router-dom"
import history from "./routes/history"
import Routes from "./routes/router"
import "./assets/scss/index.scss"

const App: React.FC = () => {
  return (
    <div className="app">
      <Router history={history}>
        <Routes />
      </Router>
    </div>
  )
}

export default App
