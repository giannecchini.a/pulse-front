import React from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import Login from "../Login/Login"
import Dashboard from "../Dashboard/Dashboard"
import { ProtectedRoute } from '../routes/auth'

const App = () => (
  <Switch>
    <Route exact={true} path="/" component={Login}/>
    <Route exact={true} path="/Login" component={Login}/>
    <Route exact={true} path="/Dashboard" component={Dashboard}/>
    <Redirect to="/"/>
  </Switch>
)

export default App
