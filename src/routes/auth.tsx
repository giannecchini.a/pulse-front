import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { storage } from "../utils/utils"

export const RedirectUnauthorized = () => <Redirect to="/Login" />
// Export const ProtectedRoute = (props: any) => storage.get().token ? <Route {...props}/> : <RedirectUnauthorized/>
export const ProtectedRoute = () => <Redirect to="/Login" />