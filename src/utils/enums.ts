
export enum ChartColors {
  calypse = '#92DCE5',
  blue = '#304269',
  orangeDark = '#F26100',
  green = '#b2c135',
  orange = '#FFA500'
}

export enum InitialRequirements {
  color = '#92DCE5',
  class = 'calypse',
  title = 'Initial Requirements',
  code = 'INI_REQ'
}

export enum DeliveryPacing {
  color = '#304269',
  class = 'blue',
  title = 'Delivery / Pacing',
  code = 'DEL_PAC'
}

export enum Segmentation {
  color = '#F26100',
  class ='orange-dark',
  title = 'Segmentation',
  code = 'SEG'
}

export enum BrandSafety {
  color = '#b2c135',
  class = 'green',
  title = 'Brand Safety',
  code = 'BRA_SAF'
}

export enum AIOptimization {
  color = '#FFA500',
  class = 'orange',
  title = 'AI Optimizations',
  code = 'AI_OPT'
}
