
const arrayDays = () => {
  const arrayLabels = []
  for (let index = 1; index < 31; index++) {
    arrayLabels.push(`${index} Ene`)
  }
  return arrayLabels
}

const requestFetch = (method: string, body: object) => {
  const header = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
  const request = {
    body: JSON.stringify(body),
    headers: header,
    method,
  }
  return request
}

const setStorage = (token: string) => {
  sessionStorage.setItem('auth', token)
}

const getStorage = () => {
  return {
    token : sessionStorage.getItem('auth'),
  }
}

const clearStorage = () => {
  sessionStorage.clear()
}

const removeStorage = () => {
  sessionStorage.removeItem('auth')
}

const storage = {
  clear: clearStorage,
  get: getStorage,
  remove: removeStorage,
  set: setStorage,
}

export {
  arrayDays,
  requestFetch,
  storage,
}
