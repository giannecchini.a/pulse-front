import React from "react"
import history from "../routes/history"
import { requestFetch, storage } from "../utils/utils"

interface ILoginState {
  token?: string
  email: string
  password: string
}

export default class Panel extends React.Component<{},ILoginState> {

  constructor(props: any) {
    super(props)
    this.state = {
      email: '',
      password: '',
      token: '',
    }
  }

  onLogin = () => {
    // This.getLogin()
    history.replace({ pathname: "/Dashboard" })
  }

  getLogin = async () => {
    try {
      const { email, password } = this.state
      const body = {
        password,
        username: email,
      }
      const request = requestFetch('POST',body)
      const response = await fetch('https://api.xaxispulse.com/api/Authentication/Login', request)
      if (response.ok) {
        const json = await response.json()
        storage.set(json.token)
        history.replace({ pathname: "/Dashboard" })
      }
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error)
    }
  }

  onChange = (type: string, e: React.FormEvent<HTMLInputElement>) => {
    switch (type){
      case 'email': this.setState({ email: e.currentTarget.value })
      break
      case 'password':this.setState({ password: e.currentTarget.value })
      break
    }
  }

  render() {
    const { email, password } = this.state
    return (
      <div className="panel-container">
        <div className="logo-pulse">
          <img alt="pulse" src="./assets/images/logo_pulse_blue.png"/>
        </div>
        <input
          className="input-test"
          name="email-form"
          title="email"
          placeholder="Email"
          value={email}
          autoComplete="off"
          onChange={(e) => { this.onChange("email",e) }}
        />
        <input
          className="input-test"
          name="password-form"
          autoComplete="off"
          type="password"
          title="password"
          placeholder="Password"
          value={password}
          onChange={(e) => { this.onChange("password",e) }}
        />
        <div className="test-space" >
          <button
            type="button"
            className="pl-button"
            onClick={this.onLogin}
          >
            LOGIN
          </button>
        </div>
      </div>
    )
  }
}
