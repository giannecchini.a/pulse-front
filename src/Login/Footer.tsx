import React from "react"

const Footer: React.FC = () => {
  return (
    <div>
      <div className="space-lines">
        <div className="line-one">
          <div className="lines one blue-dark" />
          <div className="lines two blue-light" />
          <div className="lines three gray-light" />
          <div className="lines four blue-lighter" />
        </div>
        <div className="line-two">
          <div className="lines four blue-dark" />
          <div className="lines three blue-light" />
          <div className="lines two gray-light" />
          <div className="lines one blue-lighter" />
        </div>
      </div>
      <div className="footer" />
    </div>
  )
}

export default Footer
