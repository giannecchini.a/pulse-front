import * as React from "react"
import Footer from "./Footer"
import Panel from "./Panel"

export interface ILoginState {
  login?: boolean
}

export default class Login extends React.Component<{},ILoginState> {

  constructor(props: any) {
    super(props)
    this.state = {
      login: true,
    }
  }

  render() {
    return(
      <div className="login-bg">
        <div className="space" />
        <Panel />
        <Footer />
      </div>
    )
  }
}
