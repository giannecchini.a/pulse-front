import * as React from "react"
import { slide as Menu } from "react-burger-menu"
import history from "../../routes/history"
import { storage } from '../../utils/utils'

export interface IChange {
  isOpen: boolean,
}

export interface ILoginState {
  open?: boolean
}

export default class MenuHeader extends React.Component<{},ILoginState> {

  constructor(props: any) {
    super(props)
    this.state = {
      open: false ,
    }
  }

  handleStateChange = (state: IChange) => {
    this.setState({open: state.isOpen})
  }

  closeMenu = () => {
    this.setState({open: false })
  }

  routeMenu = (path: string) => {
    history.replace({ pathname: `/${path}` })
    this.closeMenu()
  }

  logout = () => {
    storage.remove()
    history.replace({ pathname: `/Login` })
    this.closeMenu()
  }

  render() {
    const { open } = this.state
    return(
      <Menu
        onStateChange={(state) => this.handleStateChange(state)}
        isOpen={open}
        className="bm-menu"
        right={true}
        pageWrapId={"app"}
        outerContainerId={"root"}
      >
        <div className="menu-item" onClick={() => {this.routeMenu("Dashboard")}}>
            Dashboard
        </div>
        <div className="menu-item" onClick={this.logout}>
          Logout
        </div>
      </Menu>
    )
  }
}
