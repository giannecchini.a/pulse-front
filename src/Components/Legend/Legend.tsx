import React from "react"
import ReactTooltip from 'react-tooltip'

export interface ILegendProps {
  id: number
  title: string
  color: string
  tooltip: any
}

const Legend = (props: ILegendProps) => {
  const {id, color, title, tooltip } = props
  return (
    <div data-tip={true} data-for={`${id}-desc`} className="pulse-container-legend">
      <div className={`color ${color} small`}/>
      <div style={{ marginLeft: 5 }}>{title}</div>
      <ReactTooltip id={`${id}-desc`} type='info'>
        <span>{tooltip.text}</span>
      </ReactTooltip>
    </div>
  )
}

export default Legend
