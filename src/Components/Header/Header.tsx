import React from "react"

export interface IHeaderProps {
  title?: string,
  type: string,
}

const Header = (props: IHeaderProps) => {
  const {title, type} = props
  return (
    <div className="bar-header">
      <div className="space-header" >
        {type === "loading" ? (
          <div className="space-item title" />
        ): (
          <div className="space-item title">
            {title}
          </div>
        )}
        <div className="space-item logo">
          <img alt="pulse" src="./assets/images/logo_pulse_white.png"/>
        </div>
        <div className="space-item menu" />
      </div>
    </div>
  )
}

export default Header
