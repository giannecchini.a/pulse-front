import React from "react"

export interface IRowProps {
  title: string,
  detail: string,
}

const Row = (props: IRowProps) => {
  const {title, detail} = props
  return (
    <div className="title-row data">
      <div className="row-title" >
        {title}
      </div>
      <div className="row-detail">
        {detail}
      </div>
    </div>
  )
}

export default Row
