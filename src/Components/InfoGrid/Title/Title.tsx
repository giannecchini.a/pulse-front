import React from "react"

export interface IRowProps {
  title: string,
}

const Title = (props: IRowProps) => {
  const {title} = props
  return (
    <div className="title-row">
       {title}
    </div>
  )
}

export default Title
