import React from "react"
import Modal from 'react-modal'
import ReactLoading from "react-loading"

const customStyles = {
  content: {
    backgroundColor: '',
    border: 'none',
    borderStyle: 'none',
    bottom: 'auto',
    left: '50%',
    marginRight: '-50%',
    right: 'auto',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  }
}

export interface ILoaderProps {
  modalIsOpen: boolean,
}

export default class Loader extends React.Component<ILoaderProps, ILoaderProps> {

  static getDerivedStateFromProps(props: ILoaderProps, state: ILoaderProps) {
    if (props.modalIsOpen !== state.modalIsOpen) {
      return {
        modalIsOpen: props.modalIsOpen,
      }
    }
    return null
  }

  constructor(props: ILoaderProps) {
    super(props)
    this.state = {
      modalIsOpen: props.modalIsOpen
    }
  }

  public openModal = () => {
    this.setState({modalIsOpen: true})
  }

  public closeModal  = () => {
    this.setState({modalIsOpen: false })
  }

  render() {
    const { modalIsOpen } = this.props
    return (
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
          ariaHideApp={false}
        >
          <ReactLoading type="spinningBubbles" color="#304269" />
        </Modal>
    )
  }

}

