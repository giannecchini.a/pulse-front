import React from "react"
import ReactLoading from "react-loading"
import Header from '../Header/Header'
import LoadingTips from '../..//Dashboard/Tips/LoadingTips'

const InitialLoader = () => {
  return (
    <div>
        <Header title="Dashboard" type="loading" />
        <div className="pulse-content-loader" >
          <LoadingTips />
          <ReactLoading type="spinningBubbles" color="#304269" />
        </div>
    </div>
  )
}

export default InitialLoader
